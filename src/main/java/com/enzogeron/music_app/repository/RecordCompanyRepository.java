package com.enzogeron.music_app.repository;

import com.enzogeron.music_app.entity.RecordCompanyEntity;
import org.springframework.data.repository.CrudRepository;

public interface RecordCompanyRepository extends CrudRepository<RecordCompanyEntity, String>{

}
