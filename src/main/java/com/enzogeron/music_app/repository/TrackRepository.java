package com.enzogeron.music_app.repository;

import com.enzogeron.music_app.entity.TrackEntity;
import org.springframework.data.repository.CrudRepository;

public interface TrackRepository extends CrudRepository<TrackEntity, Long>{

}
