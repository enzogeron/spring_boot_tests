package com.enzogeron.music_app.service;

import com.enzogeron.music_app.entity.RecordCompanyEntity;
import com.enzogeron.music_app.service.common.SimpleService;

public interface IRecordCompanyService extends SimpleService<RecordCompanyEntity, Long> {

}
