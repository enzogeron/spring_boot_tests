package com.enzogeron.music_app.service;

import com.enzogeron.music_app.entity.TrackEntity;
import com.enzogeron.music_app.service.common.SimpleService;

public interface ITrackService extends SimpleService<TrackEntity, Long> {

}
